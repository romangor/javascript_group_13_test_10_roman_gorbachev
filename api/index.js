const express = require('express');
const news = require('./app/news.js');
const comments = require('./app/comments.js');
const database = require('./sqlDatabase');
const cors = require('cors');

const port = 8000;
const app = express();
app.use(express.json());
app.use(cors());
app.use('/news', news);
app.use('/comments', comments);


const run = async () => {
    await database.init();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}

run().catch(e => console.log(e));