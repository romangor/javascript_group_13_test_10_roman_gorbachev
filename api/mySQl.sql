create schema roman collate utf8_general_ci;
use roman;

create table news
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         not null,
    image       varchar(31)  null,
    date        varchar(63)  null
);

create table comments
(
    id      int auto_increment
        primary key,
    news_id int          not null,
    author  varchar(255) null,
    comment text         not null,
    constraint comments_news_id_fk
        foreign key (news_id) references news (id)
);

insert into news (id, title, description, image, date)
values  (1, 'Cosmos', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'c2EGepu3xAVYhkZ73kD_F.jpeg', '');


insert into comments (id, news_id, author, comment)
values  (1, 1, 'Roman', 'Hello');
