const express = require('express');
router = express.Router();
const database = require('../sqlDatabase');

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT * FROM comments WHERE news_id = ?';
        const id = parseInt(req.query.news_id);
        let [comments] = await database.getConnection().execute(query, [id]);
        return res.send(comments);
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        let query = 'INSERT INTO comments (news_id, author, comment) VALUES (?, ?, ?)';
        const comment = {
            news_id: req.body.news_id,
            author: req.body.author,
            comment: req.body.comment
        }
        await database.getConnection().execute(query, [comment.news_id, comment.author, comment.comment]);
        res.send({message: 'Added new comment'});
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        let query = 'DELETE FROM comments WHERE id = ?';
        await database.getConnection().execute(query, [req.params.id]);
        res.send({message: 'Deleted comment'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;