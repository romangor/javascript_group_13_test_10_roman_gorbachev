const express = require('express');
router = express.Router();
const database = require('../sqlDatabase')
const path = require("path");
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT id, title, image, date FROM news';
        let [news] = await database.getConnection().execute(query);
        return res.send(news);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    let query = 'SELECT * FROM news WHERE id = ?'
    let [news] = await database.getConnection().execute(query, [req.params.id]);
    const oneNews = news[0];
    return res.send(oneNews);
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title || !req.body.description) {
            return res.status(400).send({message: 'Title and description require'});
        }
        let query = 'INSERT INTO news (title, description, image, date) VALUES (?,?,?,?)';
        const oneNews = {
            title: req.body.title,
            description: req.body.description,
            image: null,
            date: req.body.date,
        };
        if (req.file) {
            oneNews.image = req.file.filename;
        }
        await database.getConnection().execute(query, [
            oneNews.title, oneNews.description, oneNews.image, oneNews.date
        ]);
        return res.send({message: 'News added'})
    } catch (e) {
        next(e);
    }

})

router.delete('/:id', async (req, res, next) => {
    try {
        let query = 'DELETE FROM news WHERE id = ?';
        await database.getConnection().execute(query, [req.params.id]);
        res.send({message: 'Deleted news'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;