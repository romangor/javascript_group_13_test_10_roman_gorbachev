const path = require('path');
const rootPath = __dirname;

module.exports = {
    mySQLConfig: {
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'roman'
    },
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads')
}