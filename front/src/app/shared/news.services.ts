import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News, NewsData } from './news.model';
import { environment as env } from '../../environments/environment';
import { map, Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsServices {
  changeNews = new Subject<News[]>()
  news!: News[];

  constructor(private http: HttpClient) {
  }

  sendNews(news: NewsData) {
    news.date = new Date().toISOString();
    const formData = new FormData();
    Object.keys(news).forEach(key => {
      formData.append(key, news[key]);
    });
    return this.http.post(env.urlAPI + '/news', formData);
  }

  fetchNews() {
    return this.http.get<{ [id: string]: News }>(env.urlAPI + '/news')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new News(data.id, data.title, data.description, data.image, data.date);
        })
      }))
      .subscribe(news => {
        this.news = news;
        this.changeNews.next(this.news.slice());
      })
  }

  fetchOneNews(id: string) {
    return this.http.get<News>(`${env.urlAPI}/news/${id}`)
      .pipe(map(result => {
        return new News(id,
          result.title,
          result.description,
          result.image,
          result.date)
      }))
  }

  deleteNews(id: string) {
    this.http.delete(`${env.urlAPI}/news/${id}`).subscribe(() => {
      this.fetchNews()
    });
  }
}
