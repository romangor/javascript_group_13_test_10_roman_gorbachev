import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { map, Subject } from 'rxjs';
import { Comments } from './comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentServices {
  changeComments = new Subject<Comments[]>()

  comments!: Comments[]

  constructor(private http: HttpClient) {
  }

  fetchComments(id: string) {
    this.http.get<{ [id: string]: Comments }>(`${env.urlAPI}/comments?news_id=${id}`)
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new Comments(data.id, data.news_id, data.author, data.comment);
        })
      }))
      .subscribe(comments => {
        this.comments = comments;
        this.changeComments.next(this.comments.slice());
      })
  }

  addComment(comment: Comments) {
    const body = {
      news_id: comment.news_id,
      author: comment.author,
      comment: comment.comment
    }
    return this.http.post(env.urlAPI + '/comments', body);
  }

  removeComment(id: string, id_news: string) {
    this.http.delete(`${env.urlAPI}/comments/${id}`).subscribe(() => {
      this.fetchComments(id_news)
    })
  }
}
