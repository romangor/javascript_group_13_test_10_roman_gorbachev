export class Comments {
  constructor(
    public id: string,
    public news_id: string,
    public author: string,
    public comment: string) {
  }
}


