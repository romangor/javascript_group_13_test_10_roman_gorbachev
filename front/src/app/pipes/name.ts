import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'name'
})
export class Name implements PipeTransform {

  transform(value: string): string {
    if (value) {
      return value;
    }
    return 'Anonymous';
  }

}
