import { Component, OnInit } from '@angular/core';
import { NewsServices } from '../shared/news.services';
import { News } from '../shared/news.model';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {

  news!: News[];
  image: string = 'http://localhost:8000/uploads/';

  constructor(private newsService: NewsServices) {
  }

  ngOnInit(): void {
    this.newsService.fetchNews();
    this.newsService.changeNews.subscribe(news => this.news = news)
  }

  getImage(imageName: string) {
    return this.image += imageName;
  }

  removeNews(id: string) {
    this.newsService.deleteNews(id);
  }

}
