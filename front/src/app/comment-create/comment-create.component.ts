import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Comments } from '../shared/comment.model';
import { CommentServices } from '../shared/comment.services';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-comment-create',
  templateUrl: './comment-create.component.html',
  styleUrls: ['./comment-create.component.sass']
})
export class CommentCreateComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  @Input() id!: string

  constructor(private commentService: CommentServices) {
  }

  ngOnInit(): void {
  }

  addComment() {
    const comment = new Comments('', this.id, this.form.value.author, this.form.value.comment);
    this.commentService.addComment(comment).subscribe(() => {
      this.commentService.fetchComments(this.id);
    })
  }

}
