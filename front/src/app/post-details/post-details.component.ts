import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewsServices } from '../shared/news.services';
import { News } from '../shared/news.model';
import { Comments } from '../shared/comment.model';
import { CommentServices } from '../shared/comment.services';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.sass']
})
export class PostDetailsComponent implements OnInit {
  oneNews!: News;
  comments!: Comments[];

  constructor(private route: ActivatedRoute,
              private newsService: NewsServices,
              private commentService: CommentServices) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = Object.values(params)[0];
      this.newsService.fetchOneNews(id).subscribe(news => {
        this.oneNews = news;
      })
      this.commentService.fetchComments(id)
      this.commentService.changeComments.subscribe(comments => {
        this.comments = comments;
      })
    })
  }

  removeComment(id: string, id_news: string) {
    this.commentService.removeComment(id, id_news)
  }

}
