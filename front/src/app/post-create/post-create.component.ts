import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NewsServices } from '../shared/news.services';
import { NewsData } from '../shared/news.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.sass']
})
export class PostCreateComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(private newsService: NewsServices,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  sendNews() {
    const news: NewsData = this.form.value;
    this.newsService.sendNews(news).subscribe(() => {
      this.newsService.fetchNews();
      void this.router.navigate(['/news']);
    });

  }
}
